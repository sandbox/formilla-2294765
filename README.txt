CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting


INTRODUCTION
---------------------
This module installs the Formilla Live Chat script to your Drupal 7 site pages. 
Formilla.com offers cloud-based live chat software for your website.  
Simply sign-up for a Formilla.com account to get started. 


REQUIREMENTS
---------------------
This module works with Drupal 7 sites.  No special requirements.  


INSTALLATION
--------------------- 
* Upload 'formilla_live_chat' directory to '/sites/all/modules/'.
* Go to your Drupal Administration panel, then to Modules and activate 
   the `FormillaLiveChat` module under the 'Other' category).
* From your Drupal Admin panel, go to Structure -> Blocks and find the
  Formilla Live Chat block under the 'Disabled' section.  Choose 'Footer' from
  the dropdown and click 'Save Blocks'. 
* Continue to the Configuration section of this readme for complete setup. 


CONFIGURATION
---------------------
* Navigate to the "FormillaLiveChat" menu, which should appear in your 
   Admin panel.  After signing up, save your Chat ID to complete your setup.
* The Formilla Live Chat button should now appear on your website pages.


TROUBLESHOOTING
---------------------
Email help@formilla.com if you have any issues with your setup or check out our
FAQs for video tutorials and more: http://www.formilla.com/live-chat-help.aspx
