<?php
/**
 * @file
 * Formilla Live Chat module admin panel.
 */

/**
 * Admin settings form.
 */
function formillalivechat_admin_settings_form($form_state) {

  $form['general'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#prefix' => '<div><h3>Formilla Live Chat Settings</h3>',
    '#suffix' => '</div>',
  );
  $form['general']['formillalivechat_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Formilla Chat ID'),
    '#size' => '35',
    '#default_value' => variable_get('formillalivechat_id', ''),
    '#required' => 0,
    '#description' => t('Enter the Chat ID you received when creating your Formilla account and save the configuration.<br/>!url',
       array(
         '!url' => l(t('If you do not have a Formilla account, SIGN UP now!'),
         'https://www.formilla.com/sign-up.aspx?u=dr', array('attributes' => array('target' => '_blank'))))),
  );

  return system_settings_form($form);
}
